use crate::IconID;
use serde_derive::{Deserialize, Serialize};

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct FavIcon {
    pub id: IconID,
    pub data: String,
    pub mime_type: String,
}

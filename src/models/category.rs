use crate::{CategoryID, UserID};
use serde_derive::{Deserialize, Serialize};

#[derive(Clone, Debug, Deserialize)]
pub struct Category {
    pub id: CategoryID,
    pub user_id: UserID,
    pub title: String,
}

#[derive(Debug, Serialize)]
pub struct CategoryInput {
    pub title: String,
}

use super::{Enclosure, Feed};
use crate::{EntryID, FeedID, UserID};
use serde_derive::{Deserialize, Serialize};

#[derive(Clone, Debug, Deserialize)]
pub struct Entry {
    pub id: EntryID,
    pub user_id: UserID,
    pub feed_id: FeedID,
    pub title: String,
    pub url: String,
    #[serde(default)]
    pub comments_url: Option<String>,
    pub author: String,
    pub content: String,
    pub hash: String,
    pub published_at: String,
    pub created_at: String,
    #[serde(default)]
    pub changed_at: Option<String>,
    pub status: String,
    pub starred: bool,
    pub feed: Feed,
    pub reading_time: i64,
    #[serde(default)]
    pub enclosures: Vec<Enclosure>,
}

#[derive(Debug, Serialize)]
pub struct EntryStateUpdate {
    pub entry_ids: Vec<EntryID>,
    pub status: String,
}

#[derive(Debug, Deserialize)]
pub struct EntryBatch {
    pub total: i64,
    pub entries: Vec<Entry>,
}

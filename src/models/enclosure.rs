use crate::{EnclosureID, EntryID, UserID};
use serde_derive::Deserialize;

#[derive(Clone, Debug, Deserialize)]
pub struct Enclosure {
    pub id: EnclosureID,
    pub user_id: UserID,
    pub entry_id: EntryID,
    pub url: String,
    pub mime_type: String,
    pub size: i64,
}

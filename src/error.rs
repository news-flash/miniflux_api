use crate::models::MinifluxError;
use thiserror::Error;

#[derive(Error, Debug)]
pub enum ApiError {
    #[error("Failed to Parse URL")]
    Url(#[from] url::ParseError),
    #[error("Failed to (de)serialize Json")]
    Json {
        source: serde_json::error::Error,
        json: String,
    },
    #[error("Http request failed")]
    Http(#[from] reqwest::Error),
    #[error("Miniflux specific error")]
    Miniflux(MinifluxError),
    #[error("Error parsing string (enum/date) returned by miniflux")]
    Parse,
}

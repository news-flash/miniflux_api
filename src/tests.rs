use crate::models::{EntryStatus, OrderBy, OrderDirection};
use crate::MinifluxApi;
use reqwest::Client;
use std::env;
use url::Url;

fn try_from_user(url: &Url) -> Result<MinifluxApi, std::env::VarError> {
    let user = env::var("MINIFLUX_USER").inspect_err(|_| {
        eprintln!("Failed to read MINIFLUX_USER");
    })?;
    let pw = env::var("MINIFLUX_PW").inspect_err(|_| {
        eprintln!("Failed to read MINIFLUX_PW");
    })?;

    Ok(MinifluxApi::new(url, user, pw))
}

fn try_from_token(url: &Url) -> Result<MinifluxApi, std::env::VarError> {
    let token = env::var("MINIFLUX_TOKEN").inspect_err(|_| {
        eprintln!("Failed to read MINIFLUX_TOKEN");
    })?;

    Ok(MinifluxApi::new_from_token(url, token))
}

fn test_setup_env() -> MinifluxApi {
    dotenv::dotenv().expect("Failed to read .env file");
    let url = env::var("MINIFLUX_URL").expect("Failed to read MINIFLUX_URL");
    let url = Url::parse(&url).unwrap();
    try_from_token(&url)
        .or_else(|_| try_from_user(&url))
        .expect("Failed to read MINIFLUX credentials")
}

#[tokio::test(flavor = "current_thread")]
async fn healthcheck() {
    let api = test_setup_env();
    api.healthcheck(&Client::new()).await.unwrap();
}

#[tokio::test(flavor = "current_thread")]
async fn current_user() {
    let api = test_setup_env();
    let user = api.get_current_user(&Client::new()).await.unwrap();
    let username = env::var("MINIFLUX_USER").unwrap();
    assert_eq!(username, user.username);
}

#[tokio::test(flavor = "current_thread")]
async fn get_feeds() {
    let api = test_setup_env();
    let feeds = api.get_feeds(&Client::new()).await.unwrap();
    assert!(!feeds.is_empty())
}

#[tokio::test(flavor = "current_thread")]
async fn get_categories() {
    let api = test_setup_env();
    let categories = api.get_categories(&Client::new()).await.unwrap();
    assert!(!categories.is_empty())
}

#[tokio::test(flavor = "current_thread")]
async fn get_entries() {
    let entry_count = 20;
    let api = test_setup_env();
    let entries = api
        .get_entries(
            Some(EntryStatus::Read),
            None,
            Some(entry_count),
            Some(OrderBy::PublishedAt),
            Some(OrderDirection::Desc),
            None,
            None,
            None,
            None,
            None,
            &Client::new(),
        )
        .await
        .unwrap();
    assert_eq!(entry_count as usize, entries.len());
}

#[tokio::test(flavor = "current_thread")]
async fn create_delete_feed() {
    let api = test_setup_env();
    let feed_url = Url::parse("https://feedforall.com/sample.xml").unwrap();

    let feed_id = api.create_feed(&feed_url, 2, &Client::new()).await.unwrap();
    let feed = api.get_feed(feed_id, &Client::new()).await.unwrap();
    assert_eq!(feed.feed_url, feed_url.as_str());

    api.delete_feed(feed_id, &Client::new()).await.unwrap();
    let error = api.get_feed(feed_id, &Client::new()).await;
    assert!(error.is_err());
}

#[tokio::test(flavor = "current_thread")]
async fn bookmark_article() {
    let api = test_setup_env();
    let article_id = 149581;
    api.toggle_bookmark(article_id, &Client::new())
        .await
        .unwrap();
}
